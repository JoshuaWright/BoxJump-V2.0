﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using BoxJumpDLL;
using System.Collections.Generic;
using System.Diagnostics;
using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace BoxJumpV2_0
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void DrawAllEnemys(Graphics g)
        {
            foreach (Enemy e in enemys) {
                e.Draw(g);
            }
        }
        private void DrawAllClouds(Graphics g)
        {
            foreach (Cloud c in clouds) {
                c.Draw(g);
            }
        }

        private void DrawBackGround_MidDay(Graphics g)
        {
            using (Brush brush = new LinearGradientBrush(this.ClientRectangle,
                                                Color.DeepSkyBlue,
                                                Color.SkyBlue,
                                                90F)) {
                g.FillRectangle(brush, ClientRectangle);
            }

            using (Brush brush = new SolidBrush(Color.Green)) {
                g.FillRectangle(brush,
                    new Rectangle(new Point(0, 313), new Size(529, 55)));
            }

            using (Pen pen = new Pen(Color.Black, 1)) {
                g.DrawLine(pen,
                    new Point(0, 313),
                    new Point(527, 313));
            }

            sun.MidDay();
            sun.reDraw(g);
        }
        private void DrawBackGround_EndDay(Graphics g)
        {
            using (Brush brush = new LinearGradientBrush(this.ClientRectangle,
                                                Color.Red,
                                                Color.Orange,
                                                90F)) {
                g.FillRectangle(brush, ClientRectangle);
            }

            using (Brush brush = new SolidBrush(Color.Green)) {
                g.FillRectangle(brush,
                    new Rectangle(new Point(0, 313), new Size(528, 55)));
            }

            using (Pen pen = new Pen(Color.Black, 1)) {
                g.DrawLine(pen,
                    new Point(0, 313),
                    new Point(527, 313));
            }

            sun.EndDay();
            sun.reDraw(g);
        }
        private void DrawBackGround_Night(Graphics g)
        {
            using (Brush brush = new LinearGradientBrush(this.ClientRectangle,
                                                Color.Black,
                                                Color.DarkBlue,
                                                100F)) {
                g.FillRectangle(brush, ClientRectangle);
            }

            using (Brush brush = new SolidBrush(Color.Green)) {
                g.FillRectangle(brush,
                    new Rectangle(new Point(0, 313), new Size(528, 55)));
            }

            using (Pen pen = new Pen(Color.Black, 1)) {
                g.DrawLine(pen,
                    new Point(0, 313),
                    new Point(527, 313));
            }

            moon.reDraw(g);
        }

        private void Game_Menu(object sender, PaintEventArgs e)
        {
            menuBt.ForeColor = backBt.ForeColor = changeTimeBt.ForeColor =
                 aboutBt.ForeColor = textColor = Color.Black;

            if (BackGrounds[backGroundChoice] == DrawBackGround_Night) {
                menuBt.ForeColor = backBt.ForeColor = changeTimeBt.ForeColor =
                    aboutBt.ForeColor = textColor = Color.Lime;
                Cloud.TimeOfDay = 2;
            }
            else if(BackGrounds[backGroundChoice] == DrawBackGround_EndDay) {
                Cloud.TimeOfDay = 1;
            }
            else {
                Cloud.TimeOfDay = 0;
            }
            
   
            BackGrounds[backGroundChoice](e.Graphics);

            using (Brush brush = new SolidBrush(textColor)) {
                e.Graphics.DrawString("BoxJump v2.0\n",
                new Font(FontFamily.GenericSansSerif, 22, FontStyle.Bold),
                brush, new Point(150, 100));
            }
        }
        private void Game_About(object sender, PaintEventArgs e)
        {
            BackGrounds[backGroundChoice](e.Graphics);

            using (Brush brush = new SolidBrush(textColor)) {
                e.Graphics.DrawString("Author:  \tJoshua Wright\n" +
                                      "Contact: \tjoshuawright1197@gmail.com\n" +
                                      "Date:    \t\t2016-11-14",
                new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold), brush
               , new Point(120, 100));
            }
        }
        private void Game_Started(object sender, PaintEventArgs e)
        {
            foreach (PerformPaints fp in PaintActions) {
                fp(e.Graphics);
            }
            using (Brush brush = new SolidBrush(textColor)) {
                e.Graphics.DrawString("SCORE: " + score * 10,
                   new Font(FontFamily.GenericSansSerif, 15, FontStyle.Bold),
                   brush, new Point(10, 10));
            }
        }
        private void GameInProgress(object sender, EventArgs e)
        {
            enemys.RemoveAll(x => x.isActive() == false);
            clouds.RemoveAll(x => x.isActive() == false);

            score += Enemy.HavePassed;
            
            if(Generate.Next(1,500) == 3 && backGroundChoice == 0) {
                clouds.Add(new Cloud());
            }
            if (Generate.Next(1, 20) == 1 &&
                GameTime.Elapsed.TotalSeconds - SpanTime >= Enemy.MinSpanTimeDelaySeconds) {
                enemys.Add(new Enemy());
                SpanTime = Convert.ToInt32(GameTime.Elapsed.TotalSeconds);

                Debug.WriteLine("AT ENEMY GENERATON");
            }

            foreach (Enemy ee in enemys.FindAll(x => x.CurrentLocation.X <= 150)) {
                if (player.Hit(ee)) {
                    isMenu = true;
                    enemys.Clear();
                    clouds.Clear();
                    if (score * 10 > Player.HighScore)
                        Player.HighScore = score * 10;
                    score = 0;
                    this.Paint += Game_Over;
                    timer.Stop();
                    Started = false;
                    menuBt.Visible = true;
                    GameTime.Reset();
                    Enemy.HavePassed = 0;

                    Debug.WriteLine("==================");

                    isMenu = false;
                    break;
                }
            }
            Refresh();
        }
        private void Game_Over(object sender, PaintEventArgs e)
        {
            BackGrounds[backGroundChoice](e.Graphics);
            using (Brush brush = new SolidBrush(textColor)) {
                e.Graphics.DrawString("     GAME OVER\nHIGHT SCORE:" + Player.HighScore,
                  new Font(FontFamily.GenericSansSerif, 18, FontStyle.Bold), brush
                 , new Point(150, 100));
            }
        }

        private void ChangeTimebt_click(object sender, EventArgs e)
        {
            backGroundChoice++;
            if (backGroundChoice > 2)
                backGroundChoice = 0;

            Refresh();
        }
        private void Aboutbt_click(object sender, EventArgs e)
        {
            Paint += Game_About;
            changeTimeBt.Visible = false;
            aboutBt.Visible = false;
            backBt.Visible = true;
            isMenu = true;
            Refresh();
        }
        private void Backbt_click(object sender, EventArgs e)
        {
            Paint += Game_Menu;
            backBt.Visible = false;
            changeTimeBt.Visible = true;
            aboutBt.Visible = true;
            isMenu = false;
            Refresh();
        }
        private void Menubt_click(object sender, EventArgs e)
        {
            Paint += Game_Menu;
            menuBt.Visible = false;
            changeTimeBt.Visible = true;
            aboutBt.Visible = true;
            Refresh();
        }

        private void InitializeComponent2()
        {
            Started = false;
            isJumping = false;
            backGroundChoice = 0;
            score = 0;
            gameFont = new Font(FontFamily.GenericSansSerif, 18, FontStyle.Bold);
            DoubleBuffered = true;
            isMenu = false;
            Paint += Game_Menu;
            GameTime = new Stopwatch();
            timer = new System.Windows.Forms.Timer();
            timer.Interval = 1;
            timer.Tick += GameInProgress;
            enemys = new List<Enemy>();
            PaintActions = new List<PerformPaints>();
            BackGrounds = new List<PerformPaints>();
            clouds = new List<Cloud>();
            Generate = new Random();
            moon = new Moon();
            sun = new Sun();
            player = new Player();


            changeTimeBt = new Label()
            {
                Text = "Change Time",
                Font = new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold),
                ForeColor = Color.Black,
                Size = new Size(90, 30),
                Location = new Point(220, 145),
                FlatStyle = FlatStyle.Flat,
                BackColor = Color.Transparent
            };
            changeTimeBt.Click += ChangeTimebt_click;

            aboutBt = new Label()
            {
                Text = "About",
                ForeColor = Color.Black,
                Font = new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold),
                Size = new Size(90, 30),
                Location = new Point(240, 175),
                FlatStyle = FlatStyle.Flat,
                BackColor = Color.Transparent
            };
            aboutBt.Click += Aboutbt_click;

            menuBt = new Label()
            {
                Text = "Menu",
                Font = new Font(FontFamily.GenericSansSerif, 16, FontStyle.Bold),
                ForeColor = Color.Black,
                Size = new Size(90, 30),
                Location = new Point(230, 165),
                FlatStyle = FlatStyle.Flat,
                BackColor = Color.Transparent,
                Visible = false
            };
            menuBt.Click += Menubt_click;

            backBt = new Label()
            {
                Text = "Back",
                Font = new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold),
                ForeColor = Color.Black,
                Size = new Size(90, 30),
                Location = new Point(120, 185),
                FlatStyle = FlatStyle.Flat,
                BackColor = Color.Transparent,
                Visible = false
            };
            backBt.Click += Backbt_click;



            Controls.Add(changeTimeBt);
            Controls.Add(aboutBt);
            Controls.Add(menuBt);
            Controls.Add(backBt);


            //loading list for backgrounds
            BackGrounds.Add(DrawBackGround_MidDay);
            BackGrounds.Add(DrawBackGround_EndDay);
            BackGrounds.Add(DrawBackGround_Night);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(704, 450);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Box Jump v2.0";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(SpaceBar_KeyPress);
            this.ResumeLayout(false);

        }

        private void PlayerJumpValidate()
        {
            player.Jump(ref isJumping);
        }

        #endregion

        private bool isMenu;
        private int score;
        private bool isJumping;
        private Font gameFont;
        private int backGroundChoice;
        private Color textColor = Color.Black;
        private Label changeTimeBt;
        private Label aboutBt;
        private Label menuBt;
        private Label backBt;
        private int SpanTime;
        private System.Windows.Forms.Timer timer;
        private Stopwatch GameTime;
        private delegate void PerformPaints(Graphics g);
        private List<Cloud> clouds;
        private List<PerformPaints> BackGrounds;
        private List<PerformPaints> PaintActions;
        private List<Enemy> enemys;
        private bool Started;
        private Player player;
        private Sun sun;
        private Moon moon;
        private Random Generate;
    }
}

