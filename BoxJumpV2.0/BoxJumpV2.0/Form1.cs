﻿using BoxJumpDLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BoxJumpV2_0
{
    public partial class Form1 : Form
    {      
        public Form1()
        {
            InitializeComponent();
            InitializeComponent2();
        }

        private void SpaceBar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ') {
                if(isMenu) {
                    return;
                }
                else if (!Started) {
                    changeTimeBt.Visible = false;
                    aboutBt.Visible = false;
                    menuBt.Visible = false;
                    Started = true;
                    Paint += Game_Started;

                    // loading list for PaintActions
                    PaintActions.Clear();
                    PaintActions.Add(BackGrounds[backGroundChoice]);
                    PaintActions.Add(DrawAllClouds);
                    PaintActions.Add(DrawAllEnemys);
                    PaintActions.Add(player.Draw);


                    GameTime.Restart();
                    timer.Start();
                    Refresh();

                    Debug.WriteLine("FIRST SPACEBAR PRESS GAME START");
                }
                else {
                    if (!isJumping)
                        Task.Factory.StartNew(PlayerJumpValidate);
                }
            }
        }
    }
}
