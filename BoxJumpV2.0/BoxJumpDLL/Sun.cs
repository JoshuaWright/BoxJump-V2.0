﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxJumpDLL
{
    public class Sun : MovingEntity
    {      
        public static Point MidDayLocation { get; set; } = new Point(420, 30);
        public static Point EndDayLocation { get; set; } = new Point(440,230);
        public static Size MidDaySzie { get; set; } = new Size(40,40);
        public static Size EndDaySzie { get; set; } = new Size(60,60);

        public Sun()
        {

            rect = new Rectangle();

            CurrentLocation = rect.Location = EndDayLocation;
            CurrnetSzie = rect.Size = EndDaySzie;
        }

        public void EndDay()
        {
            CurrentLocation = rect.Location = EndDayLocation;
            CurrnetSzie = rect.Size = EndDaySzie;
        }

        public void MidDay()
        {
            CurrentLocation = rect.Location = MidDayLocation;
            CurrnetSzie = rect.Size = MidDaySzie;
        }

        public void reDraw(Graphics g)
        {
            using (Brush brush = new SolidBrush(Color.Yellow)) {
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.FillEllipse(brush, rect);
            }
        }
    }
}
