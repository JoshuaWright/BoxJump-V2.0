﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BoxJumpDLL
{
    public class Cloud : MovingEntity
    {
        public static int TimeOfDay { get; set; }
        private Timer lifespan;
        private Random generate;
        public Point StartLocation { get; set; }
        public Size StartSzie { get; set; } = new Size(80, 30);

        public int Speed { get; } = 1;

        public Cloud()
        {
            rect = new Rectangle();
            generate = new Random();
            lifespan = new Timer();
            lifespan.Interval = 80;
            lifespan.Elapsed += timer_Event;
            TimeOfDay = 0;
            StartLocation = new Point(550, generate.Next(10, 100));
            CurrentLocation = rect.Location = StartLocation;
            CurrnetSzie = rect.Size = StartSzie;
            lifespan.Start();
        }

        public void Draw(Graphics g)
        {
            using (Brush brush = new SolidBrush(Color.White)) {
                g.FillEllipse(brush, rect);
            }
        }

        private void timer_Event(object sender, ElapsedEventArgs e)
        {
            rect.X -= Speed;
            CurrentLocation = rect.Location;

            if (rect.X <= -80) {
                lifespan.Stop();
            }
        }

        public bool isActive()
        {
            return lifespan.Enabled;
        }
    }
}
