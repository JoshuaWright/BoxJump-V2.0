﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxJumpDLL
{
    public class Moon : MovingEntity
    {
        public static Point MidNightLocation { get; set; } = new Point(420, 30);
        public static Size MidNightSzie { get; set; } = new Size(40,40);

        public Moon()
        {
            rect = new Rectangle();
            CurrentLocation = rect.Location = MidNightLocation;
            CurrnetSzie = rect.Size = MidNightSzie;
        }

        public void reDraw(Graphics g)
        {
            using (Brush brush = new SolidBrush(Color.LightGray)) {
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.FillEllipse(brush, rect);
            }
        }
    }
}
