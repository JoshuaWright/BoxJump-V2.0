﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;

using System.Windows;
using System.Timers;
using System.Threading;
using System.Threading.Tasks;

namespace BoxJumpDLL
{
    public class Player
    {
        private Rectangle rect;
        private Color BoxBrwon;
        public Point CurrentLocation { get; set; }
        public static Point StartLocation { get; set; } = new Point(50, 263);
        public static Size StartSzie { get; set; } = new Size(50, 50);
        public static Size CurrnetSzie { get; set; }
        public static int JumpHeight { get; set; } = 70;
        public static int HighScore { get; set; } = 0;



        public Player()
        {
            BoxBrwon = ColorTranslator.FromHtml("#E0A96C");
            CurrentLocation = rect.Location = StartLocation;
            CurrnetSzie = rect.Size = StartSzie;
        }

        public void Reset()
        {
            rect.Location = StartLocation;
            rect.Size = StartSzie;
        }

        public void Draw(Graphics g)
        {
            using (Brush brush = new SolidBrush(BoxBrwon)) {
                g.FillRectangle(brush, rect);
            }
            using (Pen pen = new Pen(Color.Black, 1)) {
                g.DrawRectangle(pen, rect);
            }
        }

        public void Jump(ref bool isJumping)
        {
            isJumping = true;
            int tempy = rect.Y;
            int maxHeight = tempy - JumpHeight;


            bool up = true;
            while (up) {
                if (tempy == maxHeight) {
                    up = false;
                }
                
                tempy -= 1;
                CurrentLocation = rect.Location = new Point(rect.X, tempy);
                Thread.Sleep(1);
            }
            
           // Bitmap bm = new Bitmap()

            int airtime = 0;
            while (airtime < 250) {
                airtime++;
                Thread.Sleep(1);
            }

            bool down = true;
            while (down) {
                if (rect.Location == StartLocation) {
                    down = false;
                }
                Thread.Sleep(1);
                tempy += 1;
                CurrentLocation = rect.Location = new Point(rect.X, tempy);

            }
            CurrentLocation = rect.Location = new Point(rect.Location.X, rect.Location.Y - 1);
            isJumping = false;
        }

        public bool Hit(Enemy e)
        {
            // top left
            if (e.CurrentLocation == CurrentLocation ||
                //top right
                new Point(e.CurrentLocation.X + e.CurrnetSzie.Width, e.CurrentLocation.Y)
                == new Point(CurrentLocation.X + CurrnetSzie.Width, CurrentLocation.Y) ||
                 //bottom left 
                 new Point(e.CurrentLocation.X, e.CurrentLocation.Y + e.CurrnetSzie.Height)
                == new Point(CurrentLocation.X, CurrentLocation.Y + CurrnetSzie.Height) ||
                 // bottom right
                 new Point(e.CurrentLocation.X + e.CurrnetSzie.Width,
                 e.CurrentLocation.Y + e.CurrnetSzie.Height)
                == new Point(CurrentLocation.X + CurrnetSzie.Width,
                CurrentLocation.Y + CurrnetSzie.Height)) {
                return true;
            }
            return false;


        }

       
    }
}
