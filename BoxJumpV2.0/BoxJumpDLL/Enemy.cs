﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Timers;

namespace BoxJumpDLL
{
    public class Enemy
    {
        private Rectangle rect;
        private Timer lifespan;
        public static readonly int MinSpanTimeDelaySeconds = 2;
        public static int HavePassed = 0;
        private bool hasPassed;
        public Point CurrentLocation { get; set; }
        public Point StartLocation { get; set; } = new Point(550, 283);
        public Size StartSzie { get; set; } = new Size(20, 30);
        public Size CurrnetSzie { get; set; }
        public int Speed { get; set; } = 2;

        public Enemy()
        {
            lifespan = new Timer();
            lifespan.Interval = Speed;
            lifespan.Elapsed += timer_Event;
            CurrentLocation = rect.Location = StartLocation;
            CurrnetSzie = rect.Size = StartSzie;
            lifespan.Start();
            hasPassed = false;
        }

        public void Draw(Graphics g)
        {
            using (Brush brush = new SolidBrush(Color.Red)) {
                g.FillRectangle(brush, rect);
            }
            using (Pen pen = new Pen(Color.Black, 1)) {
                g.DrawRectangle(pen, rect);
            }
        }

        private void timer_Event(object sender, ElapsedEventArgs e)
        {
            rect.X -= Speed;
            CurrentLocation = rect.Location;
            if (CurrentLocation.X < 50 && hasPassed == false) {
                HavePassed++;
                hasPassed = true;
            }
            else if (rect.X <= -20 ) {
                lifespan.Stop();
                HavePassed--;
            }
        }

        public bool isActive()
        {
            return lifespan.Enabled;
        }

        public bool isJumped()
        {
           return CurrentLocation.X < 50;
        }
    }
}
